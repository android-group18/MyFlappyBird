package us.nvb.myflappybird;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class GameThread extends Thread {
    private double averageFPS;
    private SurfaceHolder surfaceHolder;
    private GamePanel gamePanel;
    private boolean isRunning;
    public static Canvas canvas;

    public GameThread(SurfaceHolder surfaceHolder, GamePanel gamePanel) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;
    }

    @Override
    public void run() {
        long startTime;
        long timeMillis;
        long waitTime;
        /*long totalTime = 0;
        int frameCount = 0;*/
        long targetTime = 1000 / GamePanel.MAX_FPS;

        while (isRunning) {
            startTime = System.currentTimeMillis();
            canvas = null;

            try {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    this.gamePanel.update();
                    this.gamePanel.check();
                    this.gamePanel.draw(canvas);
                }
            } catch (Exception e) {
            } finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            timeMillis = System.currentTimeMillis() - startTime;
            waitTime = targetTime - timeMillis;
            try {
                if (waitTime > 0)
                    this.sleep(waitTime);
            } catch (Exception e) {
            }

            //averageFPS - maybe remove
            /*totalTime += System.currentTimeMillis() - startTime;
            frameCount++;
            if (frameCount == GamePanel.MAX_FPS) {
                averageFPS = 1000 / (totalTime / frameCount);
                frameCount = 0;
                totalTime = 0;
                System.out.println("FlappyBird: " + averageFPS + " FPS");
            }*/
        }
    }

    public void setRun(boolean b) {
        isRunning = b;
    }
}
