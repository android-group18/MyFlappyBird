package us.nvb.myflappybird.sprites;


import android.graphics.Bitmap;
import android.graphics.Canvas;

import us.nvb.myflappybird.GamePanel;

public class Bird extends GameObject {
    private static float dy_default = 5;    // dy default
    private static float dy_g = 1.5f;       // g of dy
    private static float dup = 25;          // du default

    private BirdAnimation animation;

    private float dUp;
    private long startTime;

    public Bird(Bitmap[] images) {
        x = GamePanel.WIDTH / 5;
        y = GamePanel.HEIGHT / 5;

        width = 24;
        height = 24;

        animation = new BirdAnimation(images);
        animation.setDelayTime(50);

        dy = dy_default;
        startTime = System.nanoTime();
    }

    public void reset(){
        dy_default = 5;    // dy default
        dy_g = 1.5f;       // g of dy
        dup = 25;
        y = GamePanel.HEIGHT / 4;
        dy = dy_default;
        animation.reset();
    }

    public void setUp() {
        dUp = dup;
        dy = dy_default;
    }
    
    public void speedup() {
        dy_default += 1;
        dy_g += 0.5;
        dup += 2;
    }

    public void update() {
        animation.update();
        if (y < 0)
            y = 0;

        if (dUp >= 0.2f) {
            y -= dUp / 2;
            dUp -= dUp / 3;
        } else if (y < (GamePanel.HEIGHT * 5 / 6 - height)) {
            y += dy;
            long elapsed = (System.nanoTime() - startTime) / 1000000;
            if (elapsed > 70) {
                startTime = System.nanoTime();
                dy += dy_g;
            }
        } else {
            y = GamePanel.HEIGHT * 5 / 6 - height;
            dy = 0;
        }
    }

    public void ready(){
        animation.update();
    }

    public void draw(Canvas canvas) {
        if (dUp >= 0.2f) {
            canvas.drawBitmap(animation.getFrame()[0], x, y, null);
        } else if (dy < 8) {
            canvas.drawBitmap(animation.getFrame()[1], x, y, null);
        } else if (dy < 9.5) {
            canvas.drawBitmap(animation.getFrame()[2], x, y, null);
        } else if (dy < 12) {
            canvas.drawBitmap(animation.getFrame()[3], x, y, null);
        } else {
            canvas.drawBitmap(animation.getFrame()[4], x, y, null);
        }
    }
}
