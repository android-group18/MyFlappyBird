package us.nvb.myflappybird.sprites;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class BirdAnimation {
    private Bitmap[][] frames;
    private int currentFrame;
    private long startTime;
    private long delayTime;

    public BirdAnimation(Bitmap[] lfr) {
        frames = new Bitmap[3][5];
        for (int i = 0; i < 3; i++) {
            frames[i][1] = lfr[i];
            Matrix matrix = new Matrix();

            matrix.postRotate(-20);
            frames[i][0] = Bitmap.createBitmap( lfr[i], 0, 0, 34, 24, matrix, true);
            matrix.reset();

            matrix.postRotate(30);
            frames[i][2] = Bitmap.createBitmap( lfr[i], 0, 0, 34, 24, matrix, true);
            matrix.reset();

            matrix.postRotate(60);
            frames[i][3] = Bitmap.createBitmap( lfr[i], 0, 0, 34, 24, matrix, true);
            matrix.reset();

            matrix.postRotate(90);
            frames[i][4] = Bitmap.createBitmap( lfr[i], 0, 0, 34, 24, matrix, true);
        }
        currentFrame = 0;
        delayTime = 800;
        startTime = System.currentTimeMillis();
    }

    public void setDelayTime(long d) {
        delayTime = d;
    }

    public Bitmap[] getFrame() {
        return frames[currentFrame];
    }

    public void update() {
        if (System.currentTimeMillis() - startTime > delayTime) {
            currentFrame++;
            startTime = System.currentTimeMillis();
            if(currentFrame == frames.length) {
                currentFrame = 0;
            }
        }
    }
    public void reset() {
        currentFrame = 0;
    }
}
