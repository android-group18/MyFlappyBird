package us.nvb.myflappybird.sprites;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

import java.util.ArrayList;
import java.util.Random;

import us.nvb.myflappybird.GamePanel;

public class Tubes {
    private final static float M_BT = 110;

    private Bitmap imageTop, imageBot;
    private Tube[] lTop;
    private Tube[] lBot;
    private float dx;
    private float mspeed = 2;
    private Random rd = new Random();
    boolean tr = true;

    public Tubes(Bitmap iTop, Bitmap iBot) {
        imageBot = iBot;
        imageTop = iTop;
        lTop = new Tube[2];
        lBot = new Tube[2];
        for (int i = 0; i < 2; i++) {
            lTop[i] = new Tube(52, 320);
            lTop[i].setX(GamePanel.WIDTH - 100 - 200 * i);
            lTop[i].setDx(mspeed);
            lBot[i] = new Tube(52, 320);
            lBot[i].setX(GamePanel.WIDTH - 100 - 200 * i);
            lBot[i].setDx(mspeed);
            float ytop = rd.nextFloat() * 260 - 290;
            lTop[i].setY(ytop);
            lBot[i].setY(ytop + M_BT + 320);
        }
    }

    public void reset() {
        mspeed = 2;
        tr = true;
        for (int i = 0; i < 2; i++) {
            lTop[i].setX(GamePanel.WIDTH + 100 + 200 * i);
            lTop[i].setDx(mspeed);
            lBot[i].setX(lTop[i].getX());
            lBot[i].setDx(mspeed);
            float ytop = rd.nextFloat() * 260 - 290;
            lTop[i].setY(ytop);
            lBot[i].setY(ytop + M_BT + 320);
        }
    }

    public void speedup() {
        mspeed += 1;
        for (int i = 0; i < 2; i++) {
            lTop[i].setDx(mspeed);
            lBot[i].setDx(mspeed);
        }
    }


    public void update() {
        for (int i = 0; i < 2; i++) {
            lTop[i].update();
            lBot[i].update();
            if (lTop[i].getX() < -52) {
                if (i == 0) {
                    lTop[0].setX(lTop[1].getX() + 200);
                    lBot[0].setX(lTop[0].getX());
                    float ytop = rd.nextFloat() * 260 - 290;
                    lTop[0].setY(ytop);
                    lBot[0].setY(ytop + M_BT + 320);
                } else {
                    lTop[i].setX(lTop[i - 1].getX() + 200);
                    lBot[i].setX(lTop[i].getX());
                    float ytop = rd.nextFloat() * 260 - 290;
                    lTop[i].setY(ytop);
                    lBot[i].setY(ytop + M_BT + 320);
                }
                tr = true;
            }
        }
    }

    public int check(GameObject other) {

        for (int i = 0; i < 2; i++) {
            if (other.getX() + 34 >= lTop[i].getX() && other.getX() <= lTop[i].getX() + 52) {
                RectF o = other.getRect();
                if (RectF.intersects(o, lBot[i].getRect()) ||
                        RectF.intersects(o, lTop[i].getRect()))
                    return -1;
            }
        }

        for (int i = 0; i < 2 && tr; i++) {
            if (other.getX() > lTop[i].getX() + 52) {
                tr = false;
                return 1;
            }
        }
        return 0;
    }

    public void draw(Canvas canvas) {
        for (int i = 0; i < 2; i++) {
            lTop[i].draw(canvas, imageTop);
            lBot[i].draw(canvas, imageBot);
        }
    }
}

class Tube extends GameObject {

    public Tube(float w, float h) {
        width = w;
        height = h;
    }

    public void update() {
        x -= dx;
    }

    public void draw(Canvas canvas, Bitmap image) {
        canvas.drawBitmap(image, x, y, null);
    }
}
