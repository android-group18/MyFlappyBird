package us.nvb.myflappybird.sprites;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;

import us.nvb.myflappybird.GamePanel;

public class Ground extends GameObject {
    private Bitmap image;
    private float ry;

    public Ground(Bitmap bitmap) {
        image = bitmap;
        x = 0;
        y = GamePanel.HEIGHT * 5 / 6;
        dx = 5;
        ry = y - 24;
    }

    public void update() {
        x -= dx;
        if (x < -GamePanel.WIDTH)
            x = 0;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(image, x, y, null);
        if (x < 0) {
            canvas.drawBitmap(image, x + GamePanel.WIDTH, y, null);
        }
    }

    public boolean check(GameObject other) {
        return other.getY() >= ry;
    }
}
