package us.nvb.myflappybird;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

public class GameScores {
    private int score;
    private int bestscore;
    private Bitmap[] im;
    private SharedPreferences myPrefs;

    public GameScores(Context context) {
        score = 0;
        myPrefs = context.getSharedPreferences("myFlappybird_pre", Activity.MODE_PRIVATE);
        bestscore = myPrefs.getInt("bestScore", 0);
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inScaled = false;
        //load sprites
        im = new Bitmap[10];
        im[0] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n0, opt);
        im[1] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n1, opt);
        im[2] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n2, opt);
        im[3] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n3, opt);
        im[4] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n4, opt);
        im[5] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n5, opt);
        im[6] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n6, opt);
        im[7] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n7, opt);
        im[8] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n8, opt);
        im[9] = BitmapFactory.decodeResource(context.getResources(), R.drawable.n9, opt);
    }

    public void setbest() {
        if (bestscore < score) {
            bestscore = score;
            SharedPreferences.Editor editor = myPrefs.edit();
            editor.putInt("bestScore", bestscore);
            editor.commit();
        }
        score = -score;
    }
    public void reset() {
        score = 0;
    }

    public void setScore(int score) {
        this.score += score;
    }

    public void draw(Canvas canvas) {
        if (score < 0) {

            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setTextSize(30);
            paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
            canvas.drawText("BEST:", 10, 154, paint);
            canvas.drawText("YOU:", 10, 104, paint);

            if (bestscore < 10) {
                canvas.drawBitmap(im[bestscore], 132, 120, null);
            } else if (bestscore < 100) {
                canvas.drawBitmap(im[bestscore / 10], 117, 120, null);
                canvas.drawBitmap(im[bestscore % 10], 147, 120, null);
            } else if (bestscore < 1000) {
                int c = bestscore / 10;
                canvas.drawBitmap(im[c / 10], 102, 120, null);
                canvas.drawBitmap(im[c % 10], 132, 120, null);
                canvas.drawBitmap(im[bestscore % 10], 162, 120, null);
            } else {
                int c1, c2;
                c1 = bestscore / 100;
                c2 = bestscore % 100;
                canvas.drawBitmap(im[c1 / 10], 107, 120, null);
                canvas.drawBitmap(im[c1 % 10], 137, 120, null);
                canvas.drawBitmap(im[c2 / 10], 167, 120, null);
                canvas.drawBitmap(im[c2 % 10], 197, 120, null);
            }

            int cs = -score;
            if (cs < 10) {
                canvas.drawBitmap(im[cs], 132, 70, null);
            } else if (cs < 100) {
                canvas.drawBitmap(im[cs / 10], 117, 70, null);
                canvas.drawBitmap(im[cs % 10], 147, 70, null);
            } else if (cs < 1000) {
                int c = cs / 10;
                canvas.drawBitmap(im[c / 10], 102, 70, null);
                canvas.drawBitmap(im[c % 10], 132, 70, null);
                canvas.drawBitmap(im[cs % 10], 162, 70, null);
            } else {
                int c1, c2;
                c1 = cs / 100;
                c2 = cs % 100;
                canvas.drawBitmap(im[c1 / 10], 107, 70, null);
                canvas.drawBitmap(im[c1 % 10], 137, 70, null);
                canvas.drawBitmap(im[c2 / 10], 167, 70, null);
                canvas.drawBitmap(im[c2 % 10], 197, 70, null);
            }


        } else if (score < 10) {
            canvas.drawBitmap(im[score], 132, 10, null);
        } else if (score < 100) {
            canvas.drawBitmap(im[score / 10], 117, 10, null);
            canvas.drawBitmap(im[score % 10], 147, 10, null);
        } else if (score < 1000) {
            int c = score / 10;
            canvas.drawBitmap(im[c / 10], 102, 10, null);
            canvas.drawBitmap(im[c % 10], 132, 10, null);
            canvas.drawBitmap(im[score % 10], 162, 10, null);
        } else {
            int c1, c2;
            c1 = score / 100;
            c2 = score % 100;
            canvas.drawBitmap(im[c1 / 10], 87, 10, null);
            canvas.drawBitmap(im[c1 % 10], 117, 10, null);
            canvas.drawBitmap(im[c2 / 10], 147, 10, null);
            canvas.drawBitmap(im[c2 % 10], 177, 10, null);
        }
    }

    boolean su = true;
    public boolean isSpeedup() {
        if (!su) {
            if(score == 101 || score == 501 || score == 1001) {
                su = true;
            }
            return false;
        } else if(score == 100 || score == 500 ||  score == 1000) {
            su = false;
            return true;
        } else
            return false;
    }
}
