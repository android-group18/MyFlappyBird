package us.nvb.myflappybird;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import us.nvb.myflappybird.sprites.Bird;
import us.nvb.myflappybird.sprites.Ground;
import us.nvb.myflappybird.sprites.Tubes;

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {
    public final static float WIDTH = 288;
    public final static float HEIGHT = 512;
    public final static int MAX_FPS = 40;

    public final static int GAME_START = 0;
    public final static int GAME_RUN = 1;
    public final static int GAME_OVER = 2;
    public static final int GAME_READY = 4;

    private GameThread gameThread;
    private GameScores gameScores;
    private int isRun;

    private BitmapFactory.Options opt;
    private Bitmap background, imessage, iover, istart;
    private Ground ground;
    private Bird bird;
    private Tubes tubes;

    public GamePanel(Context context) {
        super(context);
        getHolder().addCallback(this);
        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        int counter = 0;
        while (retry && counter < 1000) {
            counter++;
            try {
                gameThread.setRun(false);
                gameThread.join();
                retry = false;
                gameThread = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //options not scale image on load
        opt = new BitmapFactory.Options();
        opt.inScaled = false;
        //load sprites
        background = BitmapFactory.decodeResource(getResources(), R.drawable.bg, opt);
        imessage = BitmapFactory.decodeResource(getResources(), R.drawable.message, opt);
        iover = BitmapFactory.decodeResource(getResources(), R.drawable.gameover, opt);
        istart = BitmapFactory.decodeResource(getResources(), R.drawable.playbtn, opt);

        ground = new Ground(BitmapFactory.decodeResource(getResources(), R.drawable.ground, opt));
        Bitmap[] ibird = new Bitmap[3];
        ibird[0] = BitmapFactory.decodeResource(getResources(), R.drawable.b1, opt);
        ibird[1] = BitmapFactory.decodeResource(getResources(), R.drawable.b2, opt);
        ibird[2] = BitmapFactory.decodeResource(getResources(), R.drawable.b3, opt);
        bird = new Bird(ibird);

        tubes = new Tubes(BitmapFactory.decodeResource(getResources(), R.drawable.toptube, opt),
                BitmapFactory.decodeResource(getResources(), R.drawable.bottomtube, opt));

        gameScores = new GameScores(this.getContext());

        gameThread = new GameThread(getHolder(), this);
        gameThread.setRun(true);
        gameThread.start();
        isRun = GAME_START;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(isRun == GAME_RUN) {
            bird.setUp();

            new Thread() {
                public void run() {
                    MediaPlayer mpl = MediaPlayer.create(getContext(), R.raw.wing);
                    mpl.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();
                        }
                    });
                    mpl.start();
                }
            }.start();

        } else if(isRun == GAME_READY) {
            isRun = GAME_RUN;
        } else {
            gameScores.reset();
            bird.reset();
            tubes.reset();
            isRun = GAME_READY;
        }
        return super.onTouchEvent(event);
    }

    public void update() {
        if(isRun == GAME_RUN) {
            ground.update();
            bird.update();
            tubes.update();
        } else if(isRun == GAME_READY) {
            ground.update();
            bird.ready();
        } else if(isRun == GAME_OVER) {
            bird.update();
        }
    }

    public void check() {
        if (isRun != GAME_RUN)
            return;
        int t = tubes.check(bird);
        if( t == -1 || ground.check(bird)) {
            new Thread() {
                public void run() {
                    MediaPlayer mpo = MediaPlayer.create(getContext(), R.raw.die);
                    mpo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();
                        }
                    });
                    mpo.start();
                }
            }.start();

            isRun = GAME_OVER;
            gameScores.setbest();
        } else if (t == 1) {
            new Thread() {
                public void run() {
                    MediaPlayer mpo = MediaPlayer.create(getContext(), R.raw.point);
                    mpo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.release();
                        }
                    });
                    mpo.start();
                }
            }.start();
            gameScores.setScore(t);
            if(gameScores.isSpeedup()) {
                tubes.speedup();
                bird.speedup();
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        final float scaleFactorX = getWidth() / WIDTH;
        final float scaleFactorY = getHeight() / HEIGHT;
        if (canvas != null) {
            final int savedState = canvas.save();
            canvas.scale(scaleFactorX, scaleFactorY);
            //begin
            canvas.drawBitmap(background, 0, 0, null);
            switch (isRun) {
                case GAME_RUN:
                    bird.draw(canvas);
                    tubes.draw(canvas);
                    ground.draw(canvas);
                    gameScores.draw(canvas);
                    break;
                case GAME_OVER:
                    bird.draw(canvas);
                    tubes.draw(canvas);
                    ground.draw(canvas);
                    canvas.drawBitmap(iover, 48, 200, null);
                    canvas.drawBitmap(istart, 92, 260, null);
                    gameScores.draw(canvas);
                    break;
                case GAME_START:
                    canvas.drawBitmap(imessage, 52, 123, null);
                    break;
                case GAME_READY:
                    bird.draw(canvas);
                    ground.draw(canvas);
            }
            //end
            canvas.restoreToCount(savedState);
        }
    }
}
